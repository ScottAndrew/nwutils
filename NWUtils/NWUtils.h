//
//  NWUtils.h
//  NWUtils
//
//  Created by Scott A Andrew on 7/4/14.
//  Copyright (c) 2014 New Wave Digital Media. All rights reserved.
//


//! Project version number for NWUtils.
FOUNDATION_EXPORT double NWUtilsVersionNumber;

//! Project version string for NWUtils.
FOUNDATION_EXPORT const unsigned char NWUtilsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NWUtils/PublicHeader.h>


