//
//  NSData+NWDataAppends.m
//  NWUtils
//
//  Created by Scott A Andrew on 7/4/14.
//  Copyright (c) 2014 New Wave Digital Media. All rights reserved.
//

#import "NSMutableData+NWDataAppends.h"

@implementation NSMutableData (NWDataAppends)

-(void) appendUInt32:(uint32_t)value {
    [self appendBytes:&value length:sizeof(value)];
}

-(void) appendInt32:(int32_t)value {
    [self appendBytes:&value length:sizeof(value)];
}

-(void) appendFloat:(float)value {
    [self appendBytes:&value length:sizeof(value)];
}

-(void) appendRect:(CGRect)rect {
    [self appendFloat:CGRectGetMinX(rect)];
    [self appendFloat:CGRectGetMinY(rect)];
    [self appendFloat:CGRectGetWidth(rect)];
    [self appendFloat:CGRectGetHeight(rect)];
}

-(void) appendPoint:(CGPoint)point {
    [self appendFloat:point.x];
    [self appendFloat:point.y];
}

-(void) appendSize:(CGSize)size {
    [self appendFloat:size.width];
    [self appendFloat:size.height];
}

-(void) appendString:(NSString*)string {
    // write string out as a UTF8 string.
    //[self appendUInt32:(uint32_t)[string lengthOfBytesUsingEncoding:NSUTF8StringEncoding]+1];
    [self appendBytes:[string cStringUsingEncoding:NSUTF8StringEncoding]
               length:[string lengthOfBytesUsingEncoding:NSUTF8StringEncoding]+1];
}
@end
