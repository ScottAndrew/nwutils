//
//  NSData+NWDataAppends.h
//  NWUtils
//
//  Created by Scott A Andrew on 7/4/14.
//  Copyright (c) 2014 New Wave Digital Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableData(NWDataAppends)

-(void) appendUInt32:(uint32_t)value;
-(void) appendInt32:(int32_t)value;

-(void) appendFloat:(float)value;
-(void) appendRect:(CGRect)rect;
-(void) appendPoint:(CGPoint)point;
-(void) appendSize:(CGSize)size;
-(void) appendString:(NSString*)string;

@end
