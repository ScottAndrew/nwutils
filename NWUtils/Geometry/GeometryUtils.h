//
//  GeometryUtils.h
//  NWUtils
//
//  Created by Scott A Andrew on 7/4/14.
//  Copyright (c) 2014 New Wave Digital Media. All rights reserved.
//

CGSize CGSizeScale(CGSize size, CGFloat scaleFactor);
CGPoint CGPointSubtractFromPoint(CGPoint point1, CGPoint point2);
CGPoint CGPointAddToPoint(CGPoint point1, CGPoint point2);
CGPoint CGPointConvertToCenterOfFrame(CGPoint point, CGRect frame);
CGPoint CGPointConvertFromCenterOfFrame(CGPoint point, CGRect frame);