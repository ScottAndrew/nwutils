//
//  GeometryUtils.m
//  NWUtils
//
//  Created by Scott A Andrew on 7/4/14.
//  Copyright (c) 2014 New Wave Digital Media. All rights reserved.
//

#import "GeometryUtils.h"

CGSize CGSizeScale(CGSize size, CGFloat scaleFactor) {
    return CGSizeMake(size.width * scaleFactor, size.height * scaleFactor);
}

CGPoint CGPointSubtractFromPoint(CGPoint point1, CGPoint point2) {
    return CGPointMake(point1.x - point2.x, point1.y - point2.y);
}

CGPoint CGPointAddToPoint(CGPoint point1, CGPoint point2) {
    return CGPointMake(point1.x + point2.x, point1.y + point2.y);
}

CGPoint CGPointConvertToCenterOfFrame(CGPoint point, CGRect frame) {
    CGPoint center = CGPointMake( CGRectGetMidX(frame), CGRectGetMidY(frame));
    
    return CGPointSubtractFromPoint(point, center);
}


CGPoint CGPointConvertFromCenterOfFrame(CGPoint point, CGRect frame) {
    CGPoint center = CGPointMake( CGRectGetMidX(frame), CGRectGetMidY(frame));
    
    return CGPointAddToPoint(point, center);
}
