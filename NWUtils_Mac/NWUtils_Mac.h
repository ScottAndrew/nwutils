//
//  NWUtils_Mac.h
//  NWUtils_Mac
//
//  Created by Scott A Andrew on 8/31/14.
//  Copyright (c) 2014 New Wave Digital Media. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "NSMutableData+NWDataAppends.h"
#import "GeometryUtils.h"
